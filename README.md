## Local development

1. Инициализировать git-hooks: `./gradlew --init-script init.gradle`
2. Запустить зависимости: `docker-compose -f dependencies.yml up -d`
3. Сконфигурировать обязательные параметры и передать их через `.env` файл, либо указать в `application-local.yml`

### .env for local development

```.env
```

## Environment variables

| Параметр | Обязательный | Значение по умолчанию | Описание |
| --- | --- | --- | --- |
| LOG_LEVEL | | INFO | Уровень логирования для приложения |
| SSL_CERT_VERIFY | | normal | Настройка проверки сертификата сервера. Строка. |
| SHUTDOWN_DELAY | | 5 | Задержка перед выключением сервиса   |

### Useful info

- Генерация Jooq классов: `./gradlew generateDbJooqSchemaSource --rerun-tasks`
- Для запуска сервиса и зависимостей: `docker-compose -f app-and-dependencies.yml up -d`
- Для остановки сервиса и зависимостей: `docker-compose -f app-and-dependencies.yml down`
- Для запуска зависимостей: `docker-compose -f dependencies.yml up -d`
- Для остановки зависимостей: `docker-compose -f dependencies.yml down`
- Принудительное пересоздание всех зависимостей: `docker-compose up -f dependencies.yml --force-recreate`
- Откат изменений в liquidbase до определенного тега: `./gradlew rollback -PliquibaseCommandValue=S24-982`
- Накатить на БД изменения: `./gradlew update`