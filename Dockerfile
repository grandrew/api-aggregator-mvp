FROM openjdk:13-alpine AS build
ARG SERVICES_REPO_URL
ARG SERVICES_REPO_USER
ARG SERVICES_REPO_PASSWORD
WORKDIR /home/gradle/src
COPY build.gradle gradle.properties gradlew settings.gradle detekt.yml ./
COPY gradle/ gradle/
COPY src/ src/
RUN ./gradlew assemble --no-daemon

FROM openjdk:13-alpine
COPY --from=build /home/gradle/src/build/libs/*.jar /spring-boot-app.jar
CMD ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom" ,"spring-boot-app.jar"]
