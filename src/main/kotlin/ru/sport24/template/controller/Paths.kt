package ru.sport24.template.controller

const val SERVICE_NAME = "api-aggregator"
const val PUBLIC_PATH = "/$SERVICE_NAME"
const val PRIVATE_PATH = "/$SERVICE_NAME-admin"

const val PUBLIC_PATH_V1 = "$PUBLIC_PATH/v1"
const val PRIVATE_PATH_V1 = "$PRIVATE_PATH/v1"
