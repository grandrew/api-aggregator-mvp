package ru.sport24.template.controller.private

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.sport24.common.logs.toLogContext
import ru.sport24.template.controller.PRIVATE_PATH_V1
import ru.sport24.template.error.SomethingWentWrongException

@Tag(name = "something", description = "API doing something")
@Secured("ROLE_SPORT24_ADMIN")
@RestController
@RequestMapping(PRIVATE_PATH_V1)
class TemplatePrivateController {

    private val logger = KotlinLogging.logger {}

    @Operation(
        summary = "Save something",
        description = "Saving something when id is not 42",
    )
    @PostMapping("/{id}")
    fun saveSomething(
        @Parameter(hidden = true) authentication: Authentication,
        @PathVariable id: Int
    ): ResponseEntity<Any> {
        if (id == 42) {
            throw SomethingWentWrongException(message = "Your fish smells bad", someId = id)
        }
        id toLogContext "id"
        return ResponseEntity(HttpStatus.OK)
    }
}
