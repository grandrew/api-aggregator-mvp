package ru.sport24.template.controller.public

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.sport24.template.controller.PUBLIC_PATH_V1
import ru.sport24.template.error.SomethingWentWrongException

@Tag(name = "something", description = "API doing something")
@RestController
@RequestMapping(PUBLIC_PATH_V1)
class TemplatePublicController {

    private val logger = KotlinLogging.logger {}

    @Operation(
        summary = "Get something",
        description = "Get something when id is not 42",
    )
    @GetMapping("/{id}")
    fun getSomething(@PathVariable id: Int): ResponseEntity<Any> {
        if (id == 42) {
            throw SomethingWentWrongException(message = "Your fish smells bad", someId = id)
        }
        return ResponseEntity(HttpStatus.OK)
    }
}
