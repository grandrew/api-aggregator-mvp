package ru.sport24.template.error

class SomethingWentWrongException(message: String, cause: Throwable? = null, val someId: Int) :
    RuntimeException(message, cause)

class SomethingAnotherWrongException(message: String, cause: Throwable? = null) :
    RuntimeException(message, cause)
