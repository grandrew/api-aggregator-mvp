package ru.sport24.template

import ch.sbb.esta.openshift.gracefullshutdown.GracefulshutdownSpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan

@SpringBootApplication(exclude = [ErrorMvcAutoConfiguration::class], scanBasePackages = ["ru.sport24"])
@ConfigurationPropertiesScan
class Application

fun main() {
    GracefulshutdownSpringApplication.run(Application::class.java)
}
